<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Truck Filler</title>
</head>
<body>
    <form action="randomData.php" method="POST">
        <input type="submit" value="Generate random goods" id="gen" name="gen" />
    </form>
    <form action="filler.php" method="GET">
        <label for="maxload">Maximum load</label>
        <input type="range" id="maxload" name="maxload" min="1" max="8" value ="4" oninput="loadOutputId.value = maxload.value">
        <output name="loadOutput" id="loadOutputId" for="maxload">4</output>
        <input type="submit" value="Fill" id="fill" name="fill" />
    </form>
    <?php
        if (isset($_GET['Response'])) echo urldecode($_GET['Response']);
    ?>
</body>
</html>