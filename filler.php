<?php
    include_once 'classes/Fill.class.php';
    $maxLoad = $_GET["maxload"] ?? '';
    $returnMessage = "";
    if ($maxLoad != '' && ctype_digit($maxLoad)) {
        $fill = new Fill($maxLoad);
        $returnMessage = urlencode($fill->returnMessage);

}
if ($returnMessage != "") {
    $returnAddress = "Location: index.php?Response=$returnMessage";
}
else {
    $returnAddress = "Location: index.php";
}
header($returnAddress);
exit;