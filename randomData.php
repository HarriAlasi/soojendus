<?php
    include_once 'classes/RandomDataGenerator.class.php';
    $returnMessage = "";
    if (isset($_POST['gen'])) {
        $randomDataGenerator = new RandomDataGenerator();
        $returnMessage = urlencode($randomDataGenerator->returnMessage);
    }
    if ($returnMessage != "") {
        $returnAddress = "Location: index.php?Response=$returnMessage";
    }
    else {
        $returnAddress = "Location: index.php?";
    }
    header($returnAddress);
    exit;
