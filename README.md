
## Requirements

1. mySql 5.7 or greater
2. PHP 7 or greater

## Instructions
This application consists of two input forms, first one having just one button

1. 

Pressing the "Generate random goods" button will (re-)populate the goods table (does not have to exist) with products IDs, their weights (total weight under 100 tonnes), and truck number (initially always 0)
   
2.

* You can select the maximum load of the truck
  
* Pressing the "fill" button will "fill" a truck with products, taking the trucks maximum load into account. It tries to do so in a cost-efficient way, assuming every loading action costs money, so it will try to fill the truck with the maximum weight possible and the minimum cost possible.
  
* This will also add the contents of the truck as a separate table, using pattern "truck[truckId]", and updates relevant rows in the goods table with the truck's unique ID, truckID
  
* The overview of all the trucks "gone out" is available in the "trucks" table
