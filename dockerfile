FROM php:7.4-cli
COPY . /soojendus
WORKDIR /soojendus
CMD [ "php", "./index.php" ]