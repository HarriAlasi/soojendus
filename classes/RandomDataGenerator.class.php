<?php

include_once 'classes/Connection.class.php';
class RandomDataGenerator
{
    public $returnMessage;

    public function __construct()
    {
        $mysqli = Connection::connectToMySql();
        $this->generateRandomData($mysqli);
        $this->returnMessage = "Random Data generated!";
    }

    function generateRandomData($mysqli){
        $this->prepareTables($mysqli);
        $insertIntoQuery = "INSERT INTO goods (weight) values (?)";
        $stmt = $mysqli->prepare($insertIntoQuery);
        $totalWeight = 0;
        while ($totalWeight < 100000){
            $goodWeight = rand(55,5555);
            if ($totalWeight + $goodWeight <= 100000) {
                $stmt->bind_param("i",$goodWeight);
                $stmt->execute();
            }
            $totalWeight += $goodWeight;
        }
    }

    static function prepareTables($mysqli){
        $tableExistsQuery = "SHOW TABLES LIKE 'goods'";
        $createTableQuery = "CREATE TABLE goods (`_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, `weight` SMALLINT NOT NULL, `truck` SMALLINT)";
        $truncateTableQuery = "TRUNCATE TABLE goods";
        $result = $mysqli->query($tableExistsQuery);
        if ($result->num_rows == 1){
            $mysqli->prepare($truncateTableQuery)->execute();
            $trucksQuery = "SHOW TABLES LIKE 'truck%'";
            $trucks = $mysqli->query($trucksQuery);
            $dropTableQuery = "DROP TABLE";
            while ($table = $trucks->fetch_array()) {
                $dropTruckTableQuery = $dropTableQuery . " " . $table[0];
                $mysqli->query($dropTruckTableQuery);
            }
        } else {
            $mysqli->query($createTableQuery);
        }
    }
}