<?php


class Connection
{
    static function connectToMySql(): mysqli
    {
        $config = parse_ini_file('config/config.ini.php');
        $mysqli = new mysqli($config['db_hostname'], $config['db_user'], $config['db_pass'],$config['db_name']);
        if ($mysqli->connect_errno) {
            $_SESSION['truckMessage'] = "Failed to connect to SQL: " . $mysqli->connect_error;
            exit();
        }
        return $mysqli;
    }
}