<?php

include_once 'classes/Connection.class.php';
include_once 'classes/RandomDataGenerator.class.php';
class Fill
{
    private $maxLoad;
    public $returnMessage;

    public function __construct($maxLoad)
    {

        $mysqli = Connection::connectToMySql();
        $this->maxLoad = $maxLoad * 1000;

        $goods = $this->getGoods($mysqli);
        if (array_count_values($goods) == 0) {
            $this->returnMessage = "Truck cannot handle any of the available goods.";
        }
        $this->insertTruckLoad($mysqli, $goods);
        $mysqli->close();
    }

    function getGoods($mysqli): array{
        $arrayOfGoods = array();
        $this->prepareGoodsTableIfMissing($mysqli);
        $goodsQuery = "SELECT _id, weight FROM goods WHERE weight <= ? AND truck IS NULL ORDER BY weight DESC LIMIT 1";
        $weight = 0;
        while ($this->maxLoad > 0) {
            $stmt = $mysqli->prepare($goodsQuery);
            $stmt->bind_param("i", $this->maxLoad);
            $stmt->execute();
            $stmt->store_result();
            $id = 0;
            $stmt->bind_result($id, $weight);

            if ($stmt->num_rows > 0){
                while ($stmt->fetch()) {
                    $arrayOfGoods[] = $id;
                }
            } else {
                break;
            }
            $stmt->close();
            $this->maxLoad -= $weight;
        }
        return $arrayOfGoods;
    }

    function prepareGoodsTableIfMissing($mysqli){
        $goodsTableExistsQuery = "SHOW TABLES LIKE 'goods'";
        $result = $mysqli->query($goodsTableExistsQuery);
        if ($result->num_rows != 1){
            RandomDataGenerator::prepareTables($mysqli);
        }
    }

    function insertTruckLoad($mysqli, $goods){
        $this->prepareTrucksTable($mysqli);
        $insertTruckQuery = "INSERT INTO trucks () values ()";
        $mysqli->query($insertTruckQuery);
        $truckId = $mysqli->insert_id;
        $this->updateGoodsTableWithTruckId($mysqli, $truckId, $goods);
        $this->insertTableForTruck($mysqli, $truckId, $goods);
        $this->returnMessage = "Truck " . $truckId . " is loaded with the following goods: \n " . implode(",", $goods);

    }

    function prepareTrucksTable($mysqli){
        $tableExistsQuery = "SHOW TABLES LIKE 'trucks'";
        $createTableQuery = "CREATE TABLE trucks (`_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY)";
        if ($result = $mysqli->query($tableExistsQuery)){
            if($result->num_rows != 1){
                $mysqli->query($createTableQuery);
            }
        }
    }

    function updateGoodsTableWithTruckId($mysqli, $truckId, $goods){
        $goodsIN = "'" . join("','", $goods) . "'";
        $truckNumberForGoodsTableQuery = "UPDATE goods SET truck = ?  WHERE _id IN (" . $goodsIN . ")";
        $stmt = $mysqli->prepare($truckNumberForGoodsTableQuery);
        $stmt->bind_param("i", $truckId);
        $stmt->execute();

    }
    function insertTableForTruck($mysqli, $truckId, $goods){
        $truckTableName = "truck" . $truckId;
        $createTruckTableQuery = "CREATE TABLE " . $truckTableName . " (_id INT AUTO_INCREMENT PRIMARY KEY, good INT)";
        $insertIntoTruckQuery = "INSERT INTO " . $truckTableName . " (good) VALUES (?)";
        $stmt = $mysqli->prepare($createTruckTableQuery);
        $stmt->execute();
        foreach ($goods as $good){
            $stmt = $mysqli->prepare($insertIntoTruckQuery);
            $stmt->bind_param('i', $good);
            $stmt->execute();
        }
    }
}